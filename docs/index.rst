.. Simple comment in rst

.. toctree::
   :maxdepth: 2
   :hidden:

   second_page
   
Documentación Bluee
======================

A continuación se muestra la documentación de Bluee, con la cual podrás hacer muchas cosas interesantes con hardware

Instalación
-----------

Necesitas los siguiente

- Una bomba
- Dos baras de plutonio
- Un cabello de unicornio
- Una `raspberry`
- Y un **protoboard**

###############################
Características de la raspberry
###############################

:Modelo: Raspberry Pi
:Versión: 4
:Memoria: 4GB

Sistemas operativos
-------------------

Debes de tener instalado la versión 3 de este sistema aqui se muestra como instalarlo `Python <https://www.python.org/>`_.

En la siguiente ilustración podremos ver una breve instalación de la rapherry

.. image:: _static/_img/raspberry-pi3.jpg
   :width: 200px
   :alt: Raspberry
   :align: center